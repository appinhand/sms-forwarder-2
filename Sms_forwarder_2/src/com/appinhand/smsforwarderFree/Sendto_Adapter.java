package com.appinhand.smsforwarderFree;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.PhoneLookup;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

@SuppressLint("NewApi")
public class Sendto_Adapter extends ArrayAdapter<Contacts> {
	private final Context context;
	private final List<Contacts> values;
	DataBaseManager db;
	MsgSelection_Adapter ca;
	LinearLayout linear1;
	TextView contactname;
	TextView contactnum;
	TextView contactnum2;

	public Sendto_Adapter(Context context, int resource, List<Contacts> list_Objects) {
		super(context, R.layout.sendto_rowlayout);
		this.context = context;
		this.values = list_Objects;
		ArrayAdapter<Contacts> arrayList;
		db = new DataBaseManager(context);
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.sendto_rowlayout, parent, false);

		// ca = new MsgSelection_Adapter(context, resource, list)
		contactname = (TextView) rowView.findViewById(R.id.contactnameTextView);
		contactnum = (TextView) rowView.findViewById(R.id.contactnumTextView);

		ImageView delete_btn = (ImageView) rowView
				.findViewById(R.id.delete_btn);
		// this line
		linear1 = (LinearLayout) rowView.findViewById(R.id.linearLayout1);

		String cnum = values.get(position).getContact_number();
		String cname = getContactNameByNumber(context, cnum);

		if (cname.equals("")) {
			contactname.setText("Unknown");
			contactnum.setText(cnum);
		} else {
			contactname.setText(cname);
			contactnum.setText(cnum);
		}
		delete_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String d = values.get(position).getContact_number();
				String Query = "DELETE FROM contact_tosend WHERE \"cts_number\"=\""
						+ d + "\"";
				// holder.image_chkbox.setImageResource(R.drawable.uncheck);
				Log.e("d", Query);
				db.delete(Query);
				values.remove(position);
				notifyDataSetChanged();

				if (values.size() == 0) {
					// cl.list.setVisibility(View.INVISIBLE);
					parent.setVisibility(View.INVISIBLE);
				}
			}
		});

		return rowView;
	}

	public static String getContactNameByNumber(Context context,
			String phoneNumber) {
		// TODO Auto-generated method stub
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(phoneNumber));
		Cursor cursor = null;
		String contactName = null;
		try {
			cursor = context.getContentResolver()
					.query(uri, new String[] { PhoneLookup.DISPLAY_NAME },
							null, null, null);
		} catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (cursor != null && cursor.moveToFirst()) {
			contactName = cursor.getString(cursor
					.getColumnIndex(PhoneLookup.DISPLAY_NAME));
		} else
			contactName = "";
		cursor.close();
		return contactName;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return values.size();
	}
}
