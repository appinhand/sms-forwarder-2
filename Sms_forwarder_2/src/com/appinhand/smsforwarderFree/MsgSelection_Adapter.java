package com.appinhand.smsforwarderFree;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class MsgSelection_Adapter extends ArrayAdapter<Contacts> {
	Context context;
	List<Contacts> list;
	DataBaseManager db;
	ViewHolder holder;
	ArrayList<Boolean> tempList;

	boolean isCheck;

	// MessageModel currentModel;

	public MsgSelection_Adapter(Context context, int resource,
			List<Contacts> list) {
		super(context, R.layout.msg_rowlayout, list);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.list = list;

		db = new DataBaseManager(context);
		tempList = new ArrayList<Boolean>();

		for (int i = 0; i < this.list.size(); i++) {
			tempList.add(list.get(i).getIsChecked());
		}
	}

	static class ViewHolder {
		protected TextView txt;
		protected CheckBox chkbx;
		protected ImageView image_chkbox;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.msg_rowlayout, null);

			holder = new ViewHolder();

			holder.txt = (TextView) convertView.findViewById(R.id.TextView);
			holder.image_chkbox = (ImageView) convertView
					.findViewById(R.id.image_chkbox);

			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();

		holder.txt.setText("" + list.get(position).getName());

		if (tempList.get(position) == true) {
			holder.image_chkbox.setImageResource(R.drawable.checked);
		} else {
			holder.image_chkbox.setImageResource(R.drawable.uncheck);
		}

		holder.image_chkbox.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub

				int localPos = position;
				boolean localIsChecked = list.get(localPos).getIsChecked();

				if (!localIsChecked) {
					String Query = "INSERT INTO \"contact_msg\"(\"cm_number\") Values(\""
							+ list.get(localPos).getContact_number() + "\");";

					Log.e("query", Query);
					db.insert_update(Query);
					holder.image_chkbox
							.setBackgroundResource(R.drawable.checked);

					list.get(localPos).setIsChecked(true);
					tempList.set(localPos, true);
					Log.e("check", "" + isCheck);
					Log.e("check", "" + localPos);
					isCheck = true;
				} else {
					String Query = "DELETE FROM \"contact_msg\" WHERE \"cm_number\" =\""
							+ list.get(localPos).getContact_number() + "\";";

					Log.e("query", Query);
					db.delete(Query);
					holder.image_chkbox
							.setBackgroundResource(R.drawable.uncheck);

					Log.e("check", "" + localPos);
					Log.e("check else", "" + isCheck);
					isCheck = false;

					list.get(localPos).setIsChecked(false);
					tempList.set(localPos, false);
				}

				notifyDataSetChanged();
			}
		});

		Log.e("list is check", "" + list.get(position).getIsChecked());

		return convertView;
	}
}
