package com.appinhand.smsforwarderFree;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.R.integer;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MainActivity extends Activity {

	ToggleButton toggle;
	boolean togglebool = true;
	String toggleState = "";
	List<Contacts> contact_Arraylist;
	static DataBaseManager db;
	EditText contact_etxt;
	String contact = "";
	private static final int CONTACT_PICKER_RESULT = 10;
	Boolean validFlag;
	Boolean contactListFlag = false;
	static List<String> phoneNoList;
	ArrayList<Contacts> arrayList;
	Button phonebook_btn;
	Button save_btn;
	Button etxt_clear_btn;
	Button msg_selection_btn;
	Button clear_btn;
	Button addmore_contact_btn;
	TextView msg_selection_text;
	TextView contact_num_text;
	static String prefName = "pref";
	public static ArrayList<String> arraylist_ctsnum;
	String text;
	RelativeLayout header_layout;
	ScrollView scroll_layout;
	String cname;
	NotificationManager notificationManager;
	Animation animAccelerateDecelerate;
	Animation animAccelerateDecelerateformain;
	int i;
	EditText etxt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		db = new DataBaseManager(getApplicationContext());
		new DownloadWebPageTask().execute();
	}

	class DownloadWebPageTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {
			try {
				db.createDataBase();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {

			phoneNoList = new ArrayList<String>();
			arrayList = new ArrayList<Contacts>();
			save_btn = (Button) findViewById(R.id.save_btn);
			msg_selection_btn = (Button) findViewById(R.id.msg_selection);
			clear_btn = (Button) findViewById(R.id.etxt_clearbtn);
			msg_selection_text = (TextView) findViewById(R.id.msg_selection_text);
			contact_num_text = (TextView) findViewById(R.id.contact_num_text);
			toggle = (ToggleButton) findViewById(R.id.toggleButton1);
			etxt = (EditText) findViewById(R.id.add_contact_editText);
			notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			scroll_layout = (ScrollView) findViewById(R.id.scrollView1);
			header_layout = (RelativeLayout) findViewById(R.id.header);
			animAccelerateDecelerate = AnimationUtils.loadAnimation(
					MainActivity.this, R.anim.accelerate_decelerate);
			animAccelerateDecelerateformain = AnimationUtils.loadAnimation(
					MainActivity.this,
					R.anim.accelerate_decelerate_for_verify_btn);

			adWork();

			Bundle b = getIntent().getExtras();

			try {
				if (b != null) {

					header_layout.setVisibility(View.VISIBLE);
					scroll_layout.setVisibility(View.VISIBLE);

				} else {
					header_layout.startAnimation(animAccelerateDecelerate);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				onResume();
			} catch (Exception e) {
				// TODO: handle exception
			}

			Typeface tf = Typeface.createFromAsset(getApplicationContext()
					.getAssets(), "rock.TTF");
			msg_selection_text.setTypeface(tf);
			contact_num_text.setTypeface(tf);

			SharedPreferences appPrefs = getApplicationContext()
					.getSharedPreferences("activity", 0);
			toggleState = appPrefs.getString("toggleState", "false");
			if (toggleState.equals("true")) {
				toggle.setChecked(true);
			} else {
				toggle.setChecked(false);
			}
			final SharedPreferences.Editor editor = appPrefs.edit();

			toggle.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					togglebool = toggle.isChecked();

					if (togglebool == true) {
						Notify("SMS Forwarder", "Service :  \'ON\'");
						editor.putString("toggleState", "true");
						editor.commit();
					} else if (togglebool == false) {
						Notify("SMS Forwarder", "Service :  \'OFF\'");
						editor.putString("toggleState", "false");
						editor.commit();
					}
				}
			});

			animAccelerateDecelerate
					.setAnimationListener(new Animation.AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							header_layout.setVisibility(View.VISIBLE);
							scroll_layout.setVisibility(View.VISIBLE);
							scroll_layout
									.startAnimation(animAccelerateDecelerateformain);
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}
					});

			msg_selection_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(MainActivity.this,
							MsgSelection.class);
					startActivityForResult(intent, 123);
				}
			});

			save_btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(MainActivity.this, Sendto.class);
					startActivity(intent);
				}
			});

			Boolean allmsg = GetValueFromPrefrencesForBoolean(
					getApplicationContext(), prefName, "allmsg");
			if (allmsg == false) {
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"allmsg", true);

				msg_selection_text.setText("All Incoming Messages");
			} else {
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"allmsg", true);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_from_contct", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_otherthen_cntct", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_selctv", false);
				msg_selection_text.setText("All Incoming Messages");
			}
			Boolean msg_from_contct = GetValueFromPrefrencesForBoolean(
					getApplicationContext(), prefName, "msg_from_contct");
			if (msg_from_contct == true) {
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"allmsg", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_from_contct", true);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_otherthen_cntct", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_selctv", false);
				msg_selection_text.setText("Messages From All Contacts");
			}
			Boolean msg_otherthen_cntct = GetValueFromPrefrencesForBoolean(
					getApplicationContext(), prefName, "msg_otherthen_cntct");
			if (msg_otherthen_cntct == true) {
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"allmsg", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_from_contct", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_otherthen_cntct", true);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_selctv", false);
				msg_selection_text.setText("Messages Other than Contacts");
			}
			Boolean msg_selectv = GetValueFromPrefrencesForBoolean(
					getApplicationContext(), prefName, "msg_selctv");
			if (msg_selectv == true) {
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"allmsg", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_from_contct", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_otherthen_cntct", false);
				WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
						"msg_selctv", true);
				msg_selection_text.setText("Messages From Selected Contacts");
			}
		}
	}

	// public static void printMap(Map mp) {
	// Iterator it = mp.entrySet().iterator();
	// try {
	// arraylist_new.clear();
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// while (it.hasNext()) {
	// Map.Entry pairs = (Map.Entry) it.next();
	// arraylist_new.add((String) pairs.getKey());
	// it.remove(); // avoids a ConcurrentModificationException
	// }
	//
	// for (int i = 0; i < arraylist_new.size(); i++) {
	// String q = "INSERT INTO contact_tosend (\"cts_number\") Values(\""
	// + arraylist_new.get(i) + "\")";
	// db.insert_update(q);
	// Log.e("insert_q", "" + q);
	// }
	// }

	public static Boolean GetValueFromPrefrencesForBoolean(Context context,
			String prefName, String key) {
		SharedPreferences sp = context.getSharedPreferences(prefName,
				Context.MODE_PRIVATE);
		return sp.getBoolean(key, false);
	}

	public static void WriteToPrefrences(Context context, String prefName,
			String key, String value) {
		SharedPreferences sp = context.getSharedPreferences(prefName,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sp.edit();
		edit.putString(key, value);
		edit.commit();
	}

	public static void WriteToPrefrencesForBoolean(Context context,
			String prefName, String key, boolean value) {
		SharedPreferences sp = context.getSharedPreferences(prefName,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor edit = sp.edit();
		edit.putBoolean(key, value);
		edit.commit();
	}

	public static String GetValueFromPrefrences(Context context,
			String prefName, String key) {
		SharedPreferences sp = context.getSharedPreferences(prefName,
				Context.MODE_PRIVATE);
		return sp.getString(key, "");
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case 123:
				// textview set text
				text = data.getStringExtra("text");
				msg_selection_text.setText(text);
				break;
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {

			contact_num_text = (TextView) findViewById(R.id.contact_num_text);
			save_btn = (Button) findViewById(R.id.save_btn);
			arraylist_ctsnum = new ArrayList<String>();

			String query = "SELECT * FROM \"contact_tosend\" ";
			Log.e("querymsg", "" + query);
			Cursor b = db.selectQuery(query);
			if (b.getCount() > 0) {
				if (b.moveToFirst()) {
					do {
						String i = b.getString(b.getColumnIndex("cts_number"));
						arraylist_ctsnum.add(i);
					} while (b.moveToNext());
				}
			}

			int a = arraylist_ctsnum.size();
			Log.e("ctsnumber_count", "" + a);

			if (arraylist_ctsnum.size() == 0) {
				contact_num_text.setText("No Number");
			} else if (arraylist_ctsnum.size() == 1) {
				for (int e = 0; e < arraylist_ctsnum.size(); e++) {

					cname = getContactNameByNumber(getApplicationContext(),
							arraylist_ctsnum.get(e));

				}
				contact_num_text.setText(cname);
				save_btn = (Button) findViewById(R.id.save_btn);
			} else if (arraylist_ctsnum.size() > 1) {
				contact_num_text.setText(a + " Numbers");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void Notify(String notificationTitle, String notificationMessage) {

		@SuppressWarnings("deprecation")
		Notification notification = new Notification(R.drawable.ic_launcher,
				"SMS Forwarder", System.currentTimeMillis());
		notification.flags = Notification.FLAG_ONGOING_EVENT;

		Intent notificationIntent = new Intent(this, MainActivity.class);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(MainActivity.this, notificationTitle,
				notificationMessage, pendingIntent);
		notificationManager.notify(9999, notification);
	}

	//
	public static String getContactNameByNumber(Context context,
			String phoneNumber) {
		// TODO Auto-generated method stub
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(phoneNumber));
		Cursor cursor = null;
		String contactName = null;
		try {
			cursor = context.getContentResolver()
					.query(uri, new String[] { PhoneLookup.DISPLAY_NAME },
							null, null, null);
		} catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (cursor != null && cursor.moveToFirst()) {
			contactName = cursor.getString(cursor
					.getColumnIndex(PhoneLookup.DISPLAY_NAME));
		} else
			contactName = phoneNumber;
		cursor.close();
		return contactName;
	}

	void adWork() {
		// Admob
		String adMobPulisherId = "ca-app-pub-9381472359687969/3946905736";
		String testingDeviceId = "359918043312594";
		AdView adView;
		RelativeLayout adlayout;
		AdRequest request;
		String publisherId = adMobPulisherId;
		try {
			adlayout = (RelativeLayout) findViewById(R.id.adLayout);
			// Get Screen
			Display display = ((WindowManager) this
					.getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			int screenWidth = display.getWidth();
			Log.e("Screen Width", "" + screenWidth);

			// AdMob Size Initialisations
			if (screenWidth > 300 && screenWidth <= 320)
				adView = new AdView(this, AdSize.BANNER, publisherId);
			else
				adView = new AdView(this, AdSize.SMART_BANNER, publisherId);

			adlayout.addView(adView);

			// AdMob Request
			request = new AdRequest();

			// only for testing Devices
			request.addTestDevice(AdRequest.TEST_EMULATOR);
			request.addTestDevice(testingDeviceId);
			adView.loadAd(request);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
