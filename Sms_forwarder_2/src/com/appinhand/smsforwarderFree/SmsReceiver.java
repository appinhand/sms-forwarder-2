package com.appinhand.smsforwarderFree;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.PhoneLookup;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {

	public Context ctx;
	ArrayList<Contacts> arraylist;
	ArrayList<Contacts> selective_contact;
	String str_msg_body = "";
	String str_msg = "";
	String str_address = "";
	long time_stamp;
	String msg_time = "";
	String str = "";
	String toggleState = "";
	String start;
	String stop;
	String timefromvalue;
	String timetovalue;
	Boolean allmsg;
	Boolean msg_from_contct;
	Boolean msg_otherthen_cntct;
	Boolean msg_selectv;
	Boolean as_soon;
	Boolean specific_time;
	String str_name = "";
	// Sendto_Adapter a;
	DataBaseManager db;

	@Override
	public void onReceive(Context context, Intent intent) {

		ctx = context;
		// ---get the SMS message passed in---
		Bundle bundle = intent.getExtras();
		SmsMessage[] msgs = null;
		db = new DataBaseManager(context);
		String currenttime = null;
		arraylist = new ArrayList<Contacts>();
		selective_contact = new ArrayList<Contacts>();

		if (bundle != null) {
			// ---retrieve the SMS message received---
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];
			for (int i = 0; i < msgs.length; i++) {
				msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				str_address += msgs[i].getOriginatingAddress();
				time_stamp += msgs[i].getTimestampMillis();
				str_name += getContactNameByNumber(context, str_address);
				str += " :";
				str_msg_body += msgs[i].getMessageBody().toString();
				str += "\n";
			}
			Date d = new Date(time_stamp);
			// msg_time = getMessegeTimeMillis(time_stamp);
			Log.e("timestamp", "" + d);
			str_msg = "From: " + str_name + "  (" + str_address
					+ ")\r\nReceived at: " + d + "\r\n\r\nMessage: "
					+ str_msg_body;
			Log.e("totamsg", str_msg);
			//
			// Calendar c = Calendar.getInstance();
			// int hour = c.get(Calendar.HOUR);
			// int minute = c.get(Calendar.MINUTE);
			// int meridian = c.get(Calendar.AM_PM);
			// if (meridian == 0) {
			// currenttime = pad(hour) + ":" + pad(minute) + " am";
			// } else if (meridian == 1) {
			// currenttime = pad(hour) + ":" + pad(minute) + " pm";
			// }
			// SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
			// Date ctime = null;
			//
			// try {
			// ctime = format.parse(currenttime);
			// } catch (ParseException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			// String currenttime_pm = pad(hour)+":"+pad(minute)+" pm";

			// new StringBuilder().append(pad(hour)).append(":")
			// .append(pad(minute)).append(" am");

			// start = GetValueFromPrefrences(context, "pref", "startcode");
			// Log.e("startvalue", start);
			// stop = GetValueFromPrefrences(context, "pref", "stopcode");
			// Log.e("startvalue", stop);
			// timefromvalue = GetValueFromPrefrences(context, "pref",
			// "timefrom");
			// timetovalue = GetValueFromPrefrences(context, "pref", "timeto");
			allmsg = GetValueFromPrefrencesForBoolean(context, "pref", "allmsg");
			msg_from_contct = GetValueFromPrefrencesForBoolean(context, "pref",
					"msg_from_contct");
			msg_otherthen_cntct = GetValueFromPrefrencesForBoolean(context,
					"pref", "msg_otherthen_cntct");
			msg_selectv = GetValueFromPrefrencesForBoolean(context, "pref",
					"msg_selctv");
			// as_soon = GetValueFromPrefrencesForBoolean(context, "pref",
			// "as_soon_as");
			// specific_time = GetValueFromPrefrencesForBoolean(context, "pref",
			// "spcfc_time");
			//
			// SimpleDateFormat format2 = new SimpleDateFormat("hh:mm aa");
			// Date d1 = null;
			// Date d2 = null;
			//
			// try {
			// d1 = format2.parse(timefromvalue);
			// d2 = format2.parse(timetovalue);
			// } catch (ParseException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// if (start.equals(str_msg_body)) {
			// String q = "SELECT cts_number FROM contact_tosend";
			//
			// Cursor c1 = db.selectQuery(q);
			//
			// // for traversing
			// if (c1.getCount() > 0) {
			// if (c1.moveToFirst()) {
			// do {
			// String s = c1.getString(0);
			// arraylist.add(new Contacts(s));
			// } while (c1.moveToNext());
			//
			// Log.e("arraylist Size", "" + arraylist.size());
			// }
			// }
			//
			// Boolean isFound = false;
			// if (arraylist.size() > 0) {
			// for (int i = 0; i < arraylist.size(); i++) {
			// if (PhoneNumberUtils.compare(arraylist.get(i)
			// .getContact_number(), str_address)) {
			// isFound = true;
			// Toast.makeText(ctx, "Number(s) already exist",
			// Toast.LENGTH_SHORT).show();
			// }
			// }
			// if (!isFound) {
			// String Query =
			// "INSERT INTO \"contact_tosend\"(\"cts_number\") Values(\""
			// + str_address + "\");";
			// Log.e("query", Query);
			// db.insert_update(Query);
			// Toast.makeText(ctx, "Number(s) added",
			// Toast.LENGTH_SHORT).show();
			// }
			// } else {
			// String Query =
			// "INSERT INTO \"contact_tosend\"(\"cts_number\") Values(\""
			// + str_address + "\");";
			// Log.e("query", Query);
			// db.insert_update(Query);
			// Toast.makeText(ctx, "Number(s) added", Toast.LENGTH_SHORT)
			// .show();
			// }
			// } else if (stop.equals(str_msg_body)) {
			// String Query =
			// "DELETE FROM \"contact_tosend\" WHERE \"cts_number\" =\""
			// + str_address + "\";";
			// Log.e("query", Query);
			// db.delete(Query); }
			// else {
			SharedPreferences appPrefs = context.getSharedPreferences(
					"activity", 0);

			toggleState = appPrefs.getString("toggleState", "false");

			Log.e("toggle", toggleState);
			if (toggleState.equals("true")) {

				String q = "SELECT cts_number FROM contact_tosend";

				Cursor cur = db.selectQuery(q);

				// for traversing

				Log.e("CURSOR COUNT", "" + cur.getCount());
				if (cur.getCount() > 0) {
					if (cur.moveToFirst()) {
						do {
							String s = cur.getString(0);
							arraylist.add(new Contacts(s));
						} while (cur.moveToNext());

					}
					Log.e("arraysize2", "" + arraylist.size());
				}

				// if (as_soon == true) {
				try {
					msgsofcontacts_tosend();

				} catch (Exception e) {
					// TODO: handle exception
				}

				// } else if (specific_time == true) {
				// if (d1.before(d2)) {
				//
				// if (ctime.after(d1) && ctime.before(d2)) {
				// msgsofcontacts_tosend();
				// } else if (ctime.after(d2)) {
				// d1.setTime(d1.getTime() + 86400000);
				// d2.setTime(d2.getTime() + 86400000);
				// if (ctime.after(d1) && ctime.before(d2)) {
				// msgsofcontacts_tosend();
				// }
				// }
				//
				// }
				//
				// }
			}
			// ---display the new SMS message---
		}
	}

	// }

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	public static String GetValueFromPrefrences(Context context,
			String prefName, String key) {
		SharedPreferences sp = context.getSharedPreferences(prefName,
				Context.MODE_PRIVATE);
		return sp.getString(key, "");
	}

	public static Boolean GetValueFromPrefrencesForBoolean(Context context,
			String prefName, String key) {
		SharedPreferences sp = context.getSharedPreferences(prefName,
				Context.MODE_PRIVATE);
		return sp.getBoolean(key, false);
	}

	private void sendSMS(String phoneNumber, String message) {

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, null, null);
	}

	public boolean contactExists(Context context, String number) {
		// / number is the phone number
		Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(number));
		String[] mPhoneNumberProjection = { PhoneLookup._ID,
				PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
		Cursor cur = context.getContentResolver().query(lookupUri,
				mPhoneNumberProjection, null, null, null);
		try {
			if (cur.moveToFirst()) {
				return true;
			}
		} finally {
			if (cur != null)
				cur.close();
		}
		return false;
	}

	void msgsofcontacts_tosend() {
		if (allmsg == true) {
			for (int i = 0; i < arraylist.size(); i++) {
				String phon = arraylist.get(i).getContact_number().toString();
				sendSMS(phon, str_msg);
			}
		} else if (msg_from_contct == true) {
			boolean contactexist = contactExists(ctx, str_address);
			if (contactexist == true) {
				Log.e("arraysize", "" + arraylist.size());
				for (int i = 0; i < arraylist.size(); i++) {
					String phon = arraylist.get(i).getContact_number()
							.toString();

					sendSMS(phon, str_msg);
				}
			}

		} else if (msg_otherthen_cntct == true) {
			boolean contactexist = contactExists(ctx, str_address);
			if (contactexist == false) {
				for (int i = 0; i < arraylist.size(); i++) {
					String phon = arraylist.get(i).getContact_number()
							.toString();
					sendSMS(phon, str_msg);
				}
			}
		} else if (msg_selectv == true) {
			String q2 = "SELECT cm_number FROM contact_msg";

			Cursor curs = db.selectQuery(q2);

			// for traversing
			if (curs.getCount() > 0) {
				if (curs.moveToFirst()) {
					do {
						String s = curs.getString(0);
						selective_contact.add(new Contacts(s));
					} while (curs.moveToNext());
				}
			}
			Contacts temp = new Contacts(str_address);

			for (int i = 0; i < selective_contact.size(); i++) {
				if (PhoneNumberUtils.compare(selective_contact.get(i)
						.getContact_number(), temp.getContact_number())) {
					for (int i1 = 0; i1 < arraylist.size(); i1++) {
						String phon = arraylist.get(i1).getContact_number()
								.toString();
						sendSMS(phon, str_msg);
					}
				}
			}

			// if (selective_contact.contains(temp)) {
			// for (int i = 0; i <= arraylist.size(); i++) {
			// String phon = arraylist.get(i).getContact_number()
			// .toString();
			// sendSMS(phon, str_msg);
			// }
			//
			// }
		}
	}

	public static String getContactNameByNumber(Context context,
			String phoneNumber) {
		// TODO Auto-generated method stub
		Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(phoneNumber));
		Cursor cursor = null;
		String contactName = null;
		try {
			cursor = context.getContentResolver()
					.query(uri, new String[] { PhoneLookup.DISPLAY_NAME },
							null, null, null);
		} catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (cursor != null && cursor.moveToFirst()) {
			contactName = cursor.getString(cursor
					.getColumnIndex(PhoneLookup.DISPLAY_NAME));
		} else
			contactName = "";
		cursor.close();
		return contactName;
	}

}

// @SuppressLint("NewApi")
// public static String getMessegeTimeMillis(long timeStamp) {
// SimpleDateFormat datef, timef;
// Calendar calendar = Calendar.getInstance();
// Date date = new Date(timeStamp);
// Date time = new Date(timeStamp);
// calendar.setTime(date);
// String month = calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT,
// Locale.getDefault());
// datef = new SimpleDateFormat("dd", Locale.getDefault());
// timef = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
// return month + " " + datef.format(date) + ", " + timef.format(0);
// }

// }