package com.appinhand.smsforwarderFree;

public class Contacts {
	private String contact_number;
	private String name;
	private Boolean isChecked;

	
	public Contacts(String contact_number) {
		super();
		this.contact_number = contact_number;
	}

	public Contacts(String contact_number, String name) {
		super();
		this.contact_number = contact_number;
		this.name = name;
	}

	public Contacts(String contact_number, String name, Boolean isChecked) {
		super();
		this.contact_number = contact_number;
		this.name = name;
		this.isChecked = isChecked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Boolean isChecked) {
		this.isChecked = isChecked;
	}

	public String getContact_number() {
		return contact_number;
	}

	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}

}
