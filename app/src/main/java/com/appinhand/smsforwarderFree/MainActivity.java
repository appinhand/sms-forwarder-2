package com.appinhand.smsforwarderFree;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract.PhoneLookup;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.everything.providers.android.telephony.Sms;
import me.everything.providers.android.telephony.TelephonyProvider;
import pub.devrel.easypermissions.EasyPermissions;

import static com.appinhand.smsforwarderFree.SMS_Service.PREF_ID_SMS;

public class MainActivity extends Activity {

    public SharedPreferences myPrefSpot;
    public SharedPreferences.Editor myEditSpot;

    ToggleButton toggle;
    boolean togglebool = true;
    String toggleState = "";
    List<Contacts> contact_Arraylist;
    static DataBaseManager db;
    EditText contact_etxt;
    String contact = "";
    private static final int CONTACT_PICKER_RESULT = 10;
    Boolean validFlag;
    Boolean contactListFlag = false;
    static List<String> phoneNoList;
    ArrayList<Contacts> arrayList;
    Button phonebook_btn;
    Button save_btn;
    Button etxt_clear_btn;
    Button msg_selection_btn;
    Button clear_btn;
    Button addmore_contact_btn;
    TextView msg_selection_text;
    TextView contact_num_text;
    static String prefName = "pref";
    public static ArrayList<String> arraylist_ctsnum;
    String text;
    RelativeLayout header_layout;
    ScrollView scroll_layout;
    String cname;
    NotificationManager notificationManager;
    Animation animAccelerateDecelerate;
    Animation animAccelerateDecelerateformain;
    int i;
    EditText etxt;


    private static final int RC_PERMISSIONS = 3211;
    protected static final java.lang.String RATIONALE = "Grant all permissions otherwise app will not work";

    public static final String[] permissions = new String[]{Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_SMS,
            Manifest.permission.SEND_SMS, Manifest.permission.READ_CONTACTS};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DataBaseManager(getApplicationContext());

        myPrefSpot = getSharedPreferences("pref", MODE_PRIVATE);
        myEditSpot = myPrefSpot.edit();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (EasyPermissions.hasPermissions(this, permissions)) {
                new DownloadWebPageTask().execute();

            } else {
//                warning("Error!", RATIONALE, "OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EasyPermissions.requestPermissions(MainActivity.this, RATIONALE, RC_PERMISSIONS, permissions);
                    }
                }, 1000);
//                    }
//                }, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        finish();
//                    }
//                });
            }
        } else {
            new DownloadWebPageTask().execute();
        }
    }

    public long getLong(String key) {
        return myPrefSpot.getLong(key, 0);
    }

    public void writeLong(String key, long value) {
        myEditSpot.putLong(key, value);
        myEditSpot.commit();
    }


    public void warning(String title, String message,
                        String positiveButtonText,
                        DialogInterface.OnClickListener positiveButtonClickListener,
                        String negativeButtonText,
                        DialogInterface.OnClickListener negativeButtonClickListener) {
        // TODO Auto-generated method stub

        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_launcher)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText,
                        positiveButtonClickListener)
                .setNegativeButton(negativeButtonText,
                        negativeButtonClickListener).show();
    }

    class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            try {
                long lastSmsId = getLong(PREF_ID_SMS);
                if (lastSmsId == 0) {
                    // No prev id found, get latest sms id and save it.
                    TelephonyProvider provider = new TelephonyProvider(getApplicationContext());
                    List<Sms> listSms = provider.getSms(TelephonyProvider.Filter.INBOX).getList();
                    if (listSms.size() > 0) {
                        long latestSmsId = listSms.get(0).id;
                        writeLong(PREF_ID_SMS, latestSmsId);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                db.createDataBase();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                // e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            phoneNoList = new ArrayList<String>();
            arrayList = new ArrayList<Contacts>();
            save_btn = findViewById(R.id.save_btn);
            msg_selection_btn = findViewById(R.id.msg_selection);
            clear_btn = findViewById(R.id.etxt_clearbtn);
            msg_selection_text = findViewById(R.id.msg_selection_text);
            contact_num_text = findViewById(R.id.contact_num_text);
            toggle = findViewById(R.id.toggleButton1);
            etxt = findViewById(R.id.add_contact_editText);
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            scroll_layout = findViewById(R.id.scrollView1);
            header_layout = findViewById(R.id.header);
            animAccelerateDecelerate = AnimationUtils.loadAnimation(
                    MainActivity.this, R.anim.accelerate_decelerate);
            animAccelerateDecelerateformain = AnimationUtils.loadAnimation(
                    MainActivity.this,
                    R.anim.accelerate_decelerate_for_verify_btn);

            adWork();

            Bundle b = getIntent().getExtras();

            try {
                if (b != null) {

                    header_layout.setVisibility(View.VISIBLE);
                    scroll_layout.setVisibility(View.VISIBLE);

                } else {
                    header_layout.startAnimation(animAccelerateDecelerate);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                onResume();
            } catch (Exception e) {
                // TODO: handle exception
            }

            Typeface tf = Typeface.createFromAsset(getApplicationContext()
                    .getAssets(), "rock.TTF");
            msg_selection_text.setTypeface(tf);
            contact_num_text.setTypeface(tf);

            SharedPreferences appPrefs = getApplicationContext()
                    .getSharedPreferences("activity", 0);
            toggleState = appPrefs.getString("toggleState", "false");
            if (toggleState.equals("true")) {
                toggle.setChecked(true);
            } else {
                toggle.setChecked(false);
            }

            try {
                setStateOfSwitch(Boolean.valueOf(toggleState));
            } catch (Exception e) {
                e.printStackTrace();
            }

            final SharedPreferences.Editor editor = appPrefs.edit();

            toggle.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    togglebool = toggle.isChecked();
                    editor.putString("toggleState", String.valueOf(togglebool));
                    editor.apply();

                    setStateOfSwitch(togglebool);
                }
            });

            animAccelerateDecelerate
                    .setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            header_layout.setVisibility(View.VISIBLE);
                            scroll_layout.setVisibility(View.VISIBLE);
                            scroll_layout
                                    .startAnimation(animAccelerateDecelerateformain);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });

            msg_selection_btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(MainActivity.this,
                            MsgSelection.class);
                    startActivityForResult(intent, 123);
                }
            });

            save_btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // TODO Auto-generated method stub
                    Intent intent = new Intent(MainActivity.this, Sendto.class);
                    startActivity(intent);
                }
            });

            Boolean allmsg = GetValueFromPrefrencesForBoolean(
                    getApplicationContext(), prefName, "allmsg");
            if (allmsg == false) {
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "allmsg", true);

                msg_selection_text.setText("All Incoming Messages");
            } else {
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "allmsg", true);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_from_contct", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_otherthen_cntct", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_selctv", false);
                msg_selection_text.setText("All Incoming Messages");
            }
            Boolean msg_from_contct = GetValueFromPrefrencesForBoolean(
                    getApplicationContext(), prefName, "msg_from_contct");
            if (msg_from_contct == true) {
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "allmsg", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_from_contct", true);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_otherthen_cntct", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_selctv", false);
                msg_selection_text.setText("Messages From All Contacts");
            }
            Boolean msg_otherthen_cntct = GetValueFromPrefrencesForBoolean(
                    getApplicationContext(), prefName, "msg_otherthen_cntct");
            if (msg_otherthen_cntct == true) {
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "allmsg", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_from_contct", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_otherthen_cntct", true);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_selctv", false);
                msg_selection_text.setText("Messages Other than Contacts");
            }
            Boolean msg_selectv = GetValueFromPrefrencesForBoolean(
                    getApplicationContext(), prefName, "msg_selctv");
            if (msg_selectv == true) {
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "allmsg", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_from_contct", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_otherthen_cntct", false);
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "msg_selctv", true);
                msg_selection_text.setText("Messages From Selected Contacts");
            }
        }
    }

    private void setStateOfSwitch(boolean togglebool) {
        if (togglebool == true) {
            Notify("SMS Forwarder", "Service :  \'ON\'");

            startServiceIfNeeded();
        } else if (togglebool == false) {
            Notify("SMS Forwarder", "Service :  \'OFF\'");

            stopServiceIfNeeded();
        }
    }

    private void startServiceIfNeeded() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            startService(new Intent(getApplicationContext(), SMS_Service.class));
        }
    }

    private void stopServiceIfNeeded() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            stopService(new Intent(getApplicationContext(), SMS_Service.class));
        }
    }

    // public static void printMap(Map mp) {
    // Iterator it = mp.entrySet().iterator();
    // try {
    // arraylist_new.clear();
    // } catch (Exception e) {
    // // TODO: handle exception
    // }
    // while (it.hasNext()) {
    // Map.Entry pairs = (Map.Entry) it.next();
    // arraylist_new.add((String) pairs.getKey());
    // it.remove(); // avoids a ConcurrentModificationException
    // }
    //
    // for (int i = 0; i < arraylist_new.size(); i++) {
    // String q = "INSERT INTO contact_tosend (\"cts_number\") Values(\""
    // + arraylist_new.get(i) + "\")";
    // db.insert_update(q);
    // if(AppSettings.IS_LOG_ALLOWED) Log.e("insert_q", "" + q);
    // }
    // }

    public static Boolean GetValueFromPrefrencesForBoolean(Context context,
                                                           String prefName, String key) {
        SharedPreferences sp = context.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    public static void WriteToPrefrences(Context context, String prefName,
                                         String key, String value) {
        SharedPreferences sp = context.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static void WriteToPrefrencesForBoolean(Context context,
                                                   String prefName, String key, boolean value) {
        SharedPreferences sp = context.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static String GetValueFromPrefrences(Context context,
                                                String prefName, String key) {
        SharedPreferences sp = context.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        return sp.getString(key, "");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 123:
                    // textview set text
                    text = data.getStringExtra("text");
                    msg_selection_text.setText(text);
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        try {

            contact_num_text = findViewById(R.id.contact_num_text);
            save_btn = findViewById(R.id.save_btn);
            arraylist_ctsnum = new ArrayList<String>();

            String query = "SELECT * FROM \"contact_tosend\" ";
            if (AppSettings.IS_LOG_ALLOWED) Log.e("querymsg", "" + query);
            Cursor b = db.selectQuery(query);
            if (b.getCount() > 0) {
                if (b.moveToFirst()) {
                    do {
                        String i = b.getString(b.getColumnIndex("cts_number"));
                        arraylist_ctsnum.add(i);
                    } while (b.moveToNext());
                }
            }

            int a = arraylist_ctsnum.size();
            if (AppSettings.IS_LOG_ALLOWED) Log.e("ctsnumber_count", "" + a);

            if (arraylist_ctsnum.size() == 0) {
                contact_num_text.setText("No Number");

            } else if (arraylist_ctsnum.size() == 1) {
                cname = getContactNameByNumber(getApplicationContext(),
                        arraylist_ctsnum.get(0));

                contact_num_text.setText(Html.fromHtml(cname + "<br/><small><small>" + arraylist_ctsnum.get(0) + "</small></small>"));

                save_btn = findViewById(R.id.save_btn);
            } else if (arraylist_ctsnum.size() > 1) {
                contact_num_text.setText(a + " Numbers");
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void Notify(String notificationTitle, String notificationMessage) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                this);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Notification notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher).setTicker(notificationMessage).setWhen(System.currentTimeMillis())
                .setAutoCancel(false).setContentTitle(notificationTitle)
                .setContentText(notificationMessage).setOngoing(true).build();
        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        if (notificationMessage.contains("OFF")) {
            builder.setOngoing(false);
            notification = builder.build();
        } else {
            builder.setOngoing(true);
            notification = builder.build();
        }

        mNM.notify(9999, notification);

//            @SuppressWarnings("deprecation")
//            Notification notification = new Notification(R.drawable.ic_launcher,
//                    "SMS Forwarder", System.currentTimeMillis());
//            notification.flags = Notification.FLAG_ONGOING_EVENT;
//
//            Intent notificationIntent = new Intent(this, MainActivity.class);
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                    notificationIntent, 0);
//            notification.setLatestEventInfo(MainActivity.this, notificationTitle,
//                    notificationMessage, pendingIntent);
//            notificationManager.notify(9999, notification);
//        }
    }

    //
    public static String getContactNameByNumber(Context context,
                                                String phoneNumber) {
        // TODO Auto-generated method stub
        Uri uri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = null;
        String contactName = null;
        try {
            cursor = context.getContentResolver()
                    .query(uri, new String[]{PhoneLookup.DISPLAY_NAME},
                            null, null, null);
        } catch (IndexOutOfBoundsException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        if (cursor != null && cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(PhoneLookup.DISPLAY_NAME));
        } else
            contactName = phoneNumber;
        cursor.close();
        return contactName;
    }

    void adWork() {
        // Admob
        String adMobPulisherId = "ca-app-pub-9381472359687969/3946905736";
        String testingDeviceId = "359918043312594";
        AdView adView;
        RelativeLayout adlayout;
        AdRequest request;
        String publisherId = adMobPulisherId;
        try {
            adlayout = findViewById(R.id.adLayout);
            // Get Screen
            Display display = ((WindowManager) this
                    .getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay();
            int screenWidth = display.getWidth();
            if (AppSettings.IS_LOG_ALLOWED) Log.e("Screen Width", "" + screenWidth);

            // AdMob Size Initialisations
            if (screenWidth > 300 && screenWidth <= 320)
                adView = new AdView(this, AdSize.BANNER, publisherId);
            else
                adView = new AdView(this, AdSize.SMART_BANNER, publisherId);

            adlayout.addView(adView);

            // AdMob Request
            request = new AdRequest();

            // only for testing Devices
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            request.addTestDevice(testingDeviceId);
            adView.loadAd(request);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
//
//    /**
//     * A placeholder fragment containing a simple view.
//     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            View rootView = inflater.inflate(R.layout.fragment_main, container,
//                    false);
//            return rootView;
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, final String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, new EasyPermissions.PermissionCallbacks() {
            @Override
            public void onPermissionsGranted(int requestCode, List<String> perms) {
                if (requestCode == RC_PERMISSIONS) {
                    if (perms.size() == permissions.length) {
                        new DownloadWebPageTask().execute();
                    }
                }
            }

            @Override
            public void onPermissionsDenied(int requestCode, final List<String> perms) {
                if (requestCode == RC_PERMISSIONS) {
                    warning("Error!", "Some permissions are denied, kindly allow them", "Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EasyPermissions.requestPermissions(MainActivity.this, RATIONALE, RC_PERMISSIONS, permissions);
                        }
                    }, "Cancle", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                }
            }

            @Override
            public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

            }
        });
    }

}
