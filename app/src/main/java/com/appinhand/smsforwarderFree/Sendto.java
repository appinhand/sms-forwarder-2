package com.appinhand.smsforwarderFree;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Data;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Sendto extends Activity {
    DataBaseManager db;
    Button addcontact_btn;
    Button phonebook_btn;
    Button etxt_clearbtn;
    Button advacn_sttng;
    Button btn3;
    ImageView back_button;
    String o;
    EditText contact_etxt;
    ListView list;
    ArrayList<Contacts> arrayList;
    int PICK_CONTACT = 1;
    private static final int CONTACT_PICKER_RESULT = 10;
    Context context;
    String contact = "";
    Boolean validFlag;
    Boolean contactListFlag = false;
    static List<String> phoneNoList;
    TelephonyManager mTelephonyMgr;
    String yourNumber;

    Sendto_Adapter a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendto);

        context = this;
        mTelephonyMgr = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        addcontact_btn = (Button) findViewById(R.id.add_contact_btn);
        phonebook_btn = (Button) findViewById(R.id.phonebook_btn);
        etxt_clearbtn = (Button) findViewById(R.id.etxt_clearbtn);
        back_button = (ImageView) findViewById(R.id.header_icon_image);
        list = (ListView) findViewById(R.id.ContactlistView);
        contact_etxt = (EditText) findViewById(R.id.add_contact_editText);
        db = new DataBaseManager(getApplicationContext());
        phoneNoList = new ArrayList<String>();
        arrayList = new ArrayList<Contacts>();
        adWork();

        // yourNumber = mTelephonyMgr.getLine1Number();
        // if(AppSettings.IS_LOG_ALLOWED) Log.e("yournum", yourNumber);
        String q = "SELECT cts_number FROM contact_tosend";
        try {
            db.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Cursor c = db.selectQuery(q);

        // for traversing
        if (c.getCount() > 0) {
            if (c.moveToFirst()) {
                do {
                    String s = c.getString(0);
                    arrayList.add(new Contacts(s));
                } while (c.moveToNext());
            }
        }
        if (arrayList.size() > 0) {
            list.setVisibility(View.VISIBLE);
        }
        // \"cat_name\"=" + "\""+d+"\"
        // object of custom adapter
        a = new Sendto_Adapter(getApplicationContext(),
                R.layout.sendto_rowlayout, arrayList);
        list.setAdapter(a);

        addcontact_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                String mobile_number = contact_etxt.getText().toString();
                if (mobile_number.equals(yourNumber)) {
                    if (AppSettings.IS_LOG_ALLOWED) Log.e("mynumber", yourNumber);
                    Toast.makeText(context, "You cannot enter your number",
                            Toast.LENGTH_SHORT).show();
                } else {
                    block();
                }

                try {
                    contact = "";
                    phoneNoList.clear();
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                contact_etxt.setText("");
                ;
            }

        });
        phonebook_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI);

                startActivityForResult(contactPickerIntent,
                        CONTACT_PICKER_RESULT);
            }
        });
        etxt_clearbtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                contact_etxt.setText("");
                try {
                    contact = "";
                    phoneNoList.clear();
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });

        back_button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Bundle b = new Bundle();
                b.putBoolean("sendto_back", true);
                Intent i = new Intent(Sendto.this, MainActivity.class);
                i.putExtras(b);
                startActivity(i);
                finish();

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CONTACT_PICKER_RESULT:
                    // handle contact results
                    String id = null;
                    Cursor crsr = null;
                    try {
                        Uri result = data.getData();
                        if (AppSettings.IS_LOG_ALLOWED)
                            Log.i("DEBUG_TAG", "Got a result: " + result.toString());
                        crsr = getContentResolver().query(result, null, null, null,
                                null);
                        if (AppSettings.IS_LOG_ALLOWED)
                            Log.i("DEBUG_TAG", "Cursor: " + crsr.toString());
                        while (crsr.moveToNext()) {
                            id = crsr
                                    .getString(crsr
                                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                            if (AppSettings.IS_LOG_ALLOWED) Log.e("DEBUG_TAG", "ContactID: " + id);
                        }
                        if (id != null) {
                            getContactDetailByContactId(id);
                            contactListFlag = true;
                        } else
                            Toast.makeText(context, "Couldn't Find Details",
                                    Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    } finally {
                        try {
                            crsr.close();
                        } catch (Exception e2) {
                            // TODO: handle exception
                        }
                    }

                    break;
            }
        }
    }

    //

    private void getContactDetailByContactId(String contactId) {
        // TODO Auto-generated method stub
        String[] projection = {
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};
        String selection = Data.CONTACT_ID + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(contactId)};
        String phoneNo = null, name = null, iD = null;
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    projection, selection, selectionArgs, null);

            while (cursor.moveToNext()) {
                iD = cursor
                        .getString(cursor
                                .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                name = cursor
                        .getString(cursor
                                .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                phoneNo = cursor
                        .getString(cursor
                                .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                // photoUri = cursor.getString(cursor
                // .getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
                // if(AppSettings.IS_LOG_ALLOWED) Log.e(DEBUG_TAG, "ContactId: " + iD);
                // if(AppSettings.IS_LOG_ALLOWED) Log.e(DEBUG_TAG, "Name: " + name);
                // if(AppSettings.IS_LOG_ALLOWED) Log.e(DEBUG_TAG, "PhoneNo: " + phoneNo);
                // if(AppSettings.IS_LOG_ALLOWED) Log.e(DEBUG_TAG, "photoUri: " + photoUri);
            }

            contact += name + ",";
            // edittext to display contact name
            contact_etxt.setText(contact);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null)
                cursor.close();
        }
    }

    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putBoolean("sendto_back", true);
        Intent i = new Intent(Sendto.this, MainActivity.class);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    public void block() {
        try {
            String number = contact_etxt.getText().toString();
            if (!TextUtils.isEmpty(number.trim())) {
                String query = "";
                String query2 = "";
                Cursor localCursor;
                Cursor localCursor2;

                String[] numberContacts = contact_etxt.getText().toString()
                        .trim().split(",");

                if (!contactListFlag) {
                    for (String str : numberContacts) {
                        if (isPhoneNoValid(/*normalizeNumber*/(str.trim()))) {
                            phoneNoList.add(/*normalizeNumber*/(str.trim()));
                            validFlag = true;
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Please enter valid phone number",
                                    Toast.LENGTH_LONG).show();
                            validFlag = false;
                        }
                    }
                } else {

                    // Some Numbers are from Phonebook
                    for (int i = 0; i < numberContacts.length; i++) {
                        if (phoneNoList.contains(numberContacts[i])) {
                        } else {
                            String str = getContactNumberByName(
                                    getApplicationContext(), numberContacts[i]);

                            if (isPhoneNoValid(/*normalizeNumber*/(str.trim()))) {
                                phoneNoList.add(/*normalizeNumber*/(str.trim()));
                                validFlag = true;
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Please enter valid phone number",
                                        Toast.LENGTH_LONG).show();
                                validFlag = false;
                            }
                        }
                    }
                }
                if (validFlag) {

                    for (int i = 0; i < phoneNoList.size(); i++) {
                        // Db//
                        try {
                            query = "SELECT cts_number FROM contact_tosend WHERE cts_number="
                                    + "\"" + phoneNoList.get(i) + "\"";

                            if (AppSettings.IS_LOG_ALLOWED) Log.e("query", query);

                            localCursor = db.selectQuery(query);

                            if (AppSettings.IS_LOG_ALLOWED) Log.e("LC", "" + localCursor);

                            if (localCursor.getCount() == 0) {
                                list.setVisibility(View.VISIBLE);

                                String text = phoneNoList.get(i);
                                char first_num = text.charAt(0);

//                                if (first_num == '0') {
//                                if (text.startsWith("00")) {
//                                    query = "INSERT INTO contact_tosend (\"cts_number\") Values(\""
//                                            + phoneNoList.get(i) + "\")";
//
//                                    db.insert_update(query);
//                                    if (AppSettings.IS_LOG_ALLOWED) Log.e("insertquery", query);
//                                    contact_etxt.clearComposingText();
////                                    }
//                                } else if (first_num == '+') {
//                                    query = "INSERT INTO contact_tosend (\"cts_number\") Values(\"00"
//                                            + phoneNoList.get(i).substring(1) + "\")";
//
//                                    db.insert_update(query);
//                                    if (AppSettings.IS_LOG_ALLOWED) Log.e("insertquery", query);
//                                    contact_etxt.clearComposingText();
//
//                                } else {
                                    query = "INSERT INTO contact_tosend (\"cts_number\") Values(\""
                                            + phoneNoList.get(i) + "\")";

                                    db.insert_update(query);
                                    if (AppSettings.IS_LOG_ALLOWED) Log.e("insertquery", query);
                                    contact_etxt.clearComposingText();
//                                }

                                // Toast.makeText(getApplicationContext(),
                                // "Number(s) added", Toast.LENGTH_LONG)
                                // .show();

                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Number(s) already exist",
                                        Toast.LENGTH_LONG).show();
                            }
                            try {
                                arrayList.clear();
                            } catch (Exception e) {
                                // TODO: handle exception
                            }

                            String q = "SELECT cts_number FROM contact_tosend";

                            Cursor c = db.selectQuery(q);

                            // for traversing
                            if (c.getCount() > 0) {

                                do {
                                    String s = c.getString(0);
                                    if (AppSettings.IS_LOG_ALLOWED) Log.e("number store", s);
                                    arrayList.add(new Contacts(s));
                                } while (c.moveToNext());
                                a.notifyDataSetChanged();

                            }
                        } catch (Exception e) {
                            // TODO: handle exception
                            Toast.makeText(getApplicationContext(),
                                    "Problem occured! Try again.",
                                    Toast.LENGTH_LONG).show();

                            e.printStackTrace();
                        }
                    }

                    contact_etxt.setText("");
                    phoneNoList.clear();
                    contact = "";
                }
            } else {
                Toast.makeText(getApplicationContext(),
                        "Please enter any number", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    /**
//     * Implementation copied from {@link PhoneNumberUtils#normalizeNumber}
//     */
//    private static String normalizeNumberInternal(String phoneNumber) {
//        if (TextUtils.isEmpty(phoneNumber)) {
//            return "";
//        }
//        StringBuilder sb = new StringBuilder();
//        int len = phoneNumber.length();
//        for (int i = 0; i < len; i++) {
//            char c = phoneNumber.charAt(i);
//            // Character.digit() supports ASCII and Unicode digits (fullwidth, Arabic-Indic, etc.)
//            int digit = Character.digit(c, 10);
//            if (digit != -1) {
//                sb.append(digit);
//            } else if (sb.length() == 0 && c == '+') {
//                sb.append(c);
//            } else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
//                return normalizeNumber(PhoneNumberUtils.convertKeypadLettersToDigits(phoneNumber));
//            }
//        }
//        return sb.toString();
//    }

//    public static String normalizeNumber(String phoneNumber) {
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////            return PhoneNumberUtils.normalizeNumber(phoneNumber);
////        } else {
////            return normalizeNumberInternal(phoneNumber);
////        }
//        return phoneNumber;
//    }


    public static boolean isPhoneNoValid(String number) {

        number = number.replace(" ", "")
//                .replaceAll("\\+", "")
//                .replaceAll("\\(", "")
//                .replaceAll("\\)", "")
//                .replaceAll(".", "")
//                                    .replaceAll("P","")
//                                    .replaceAll("W","")
                .replaceAll("-", "");


        boolean isValid = false;

        char[] numbersArray = number.toCharArray();

        for (int i = 0; i < numbersArray.length; i++) {
            if (Character.isDigit(numbersArray[i])) {
                isValid = true;
            } else {
                if (i != 0) // Starts with +
                    return false;
            }
        }

        return isValid;

//        return android.util.Patterns.PHONE.matcher(number).matches();
    }

    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static String getContactNumberByName(Context context,
                                                String contactName) {
        // TODO Auto-generated method stub

        Cursor cursor = null;
        String contactNumber = null;
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};
        String selection = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                + " = ?";
        String[] selectionArgs = new String[]{String.valueOf(contactName)};
        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                + " COLLATE LOCALIZED ASC";
        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, sortOrder);
        } catch (IndexOutOfBoundsException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        if (cursor != null && cursor.moveToFirst()) {
            contactNumber = cursor
                    .getString(cursor
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        } else
            contactNumber = contactName;
        cursor.close();
        return contactNumber;
    }

    void adWork() {
        // Admob
        String adMobPulisherId = "ca-app-pub-9381472359687969/3946905736";
        String testingDeviceId = "359918043312594";
        AdView adView;
        RelativeLayout adlayout;
        AdRequest request;
        String publisherId = adMobPulisherId;
        try {
            adlayout = (RelativeLayout) findViewById(R.id.adLayout);
            // Get Screen
            Display display = ((WindowManager) this
                    .getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay();
            int screenWidth = display.getWidth();
            if (AppSettings.IS_LOG_ALLOWED) Log.e("Screen Width", "" + screenWidth);

            // AdMob Size Initialisations
            if (screenWidth > 300 && screenWidth <= 320)
                adView = new AdView(this, AdSize.BANNER, publisherId);
            else
                adView = new AdView(this, AdSize.SMART_BANNER, publisherId);

            adlayout.addView(adView);

            // AdMob Request
            request = new AdRequest();

            // only for testing Devices
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            request.addTestDevice(testingDeviceId);
            adView.loadAd(request);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
