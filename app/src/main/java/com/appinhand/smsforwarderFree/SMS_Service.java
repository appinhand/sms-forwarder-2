package com.appinhand.smsforwarderFree;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import me.everything.providers.android.telephony.Sms;
import me.everything.providers.android.telephony.TelephonyProvider;
import pub.devrel.easypermissions.EasyPermissions;

import static com.appinhand.smsforwarderFree.SmsReceiver.contactExists;

public class SMS_Service extends Service {
    public static final String PREF_ID_SMS = "id_sms";
    private Context ctx;
    private Handler handler;

    ArrayList<Contacts> arraylist;
    ArrayList<Contacts> selective_contact;

    Boolean allmsg;
    Boolean msg_from_contct;
    Boolean msg_otherthen_cntct;
    Boolean msg_selectv;

    static SharedPreferences myPrefSpot;
    static SharedPreferences.Editor myEditSpot;
    private ScheduledExecutorService scheduledExecutorService_SMSSending;
    private DataBaseManager db;

    public SMS_Service() {
    }

    public static Boolean GetValueFromPrefrencesForBoolean(Context ctx,
                                                           String prefName, String key) {
        SharedPreferences sp = ctx.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        handler = new Handler(this.getMainLooper());

        ctx = SMS_Service.this;
        myPrefSpot = ctx.getSharedPreferences("pref", MODE_PRIVATE);
        myEditSpot = myPrefSpot.edit();
        db = new DataBaseManager(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (EasyPermissions.hasPermissions(this, MainActivity.permissions)) {

                startExecutor();

            } else {
//                warning("Error!", RATIONALE, "OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        EasyPermissions.requestPermissions(MainActivity.this, RATIONALE, RC_PERMISSIONS, permissions);
//                    }
//                }, "Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
                Toast.makeText(ctx, MainActivity.RATIONALE, Toast.LENGTH_SHORT).show();
                stopSelf();
            }
        } else {
            startExecutor();
        }

        return START_STICKY_COMPATIBILITY;
    }

    void startExecutor() {
        scheduledExecutorService_SMSSending = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService_SMSSending.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                long lastSmsId = getLong(PREF_ID_SMS);
                if (lastSmsId == 0) {
                    // No prev id found, get latest sms id and save it.
                    TelephonyProvider provider = new TelephonyProvider(ctx);
                    List<Sms> listSms = provider.getSms(TelephonyProvider.Filter.INBOX).getList();
                    long latestSmsId = listSms.get(listSms.size() - 1).id;
                    writeLong(PREF_ID_SMS, latestSmsId);
                } else {
                    listenAndSendSMS();
                }
            }
//        }, 0, TIME_SEND_THREAD, TimeUnit.MINUTES);
        }, 0, 5, TimeUnit.SECONDS);
    }

    private void listenAndSendSMS() {
        TelephonyProvider provider = new TelephonyProvider(ctx);
        List<Sms> sms_from_device = provider.getSms(TelephonyProvider.Filter.INBOX, getLong(PREF_ID_SMS)).getList();


        if (sms_from_device.size() > 0) {
            arraylist = new ArrayList<>();
            allmsg = GetValueFromPrefrencesForBoolean(ctx, "pref", "allmsg");
            msg_from_contct = GetValueFromPrefrencesForBoolean(ctx, "pref",
                    "msg_from_contct");
            msg_otherthen_cntct = GetValueFromPrefrencesForBoolean(ctx,
                    "pref", "msg_otherthen_cntct");
            msg_selectv = GetValueFromPrefrencesForBoolean(ctx, "pref",
                    "msg_selctv");

            SharedPreferences appPrefs = ctx.getSharedPreferences(
                    "activity", 0);

            String toggleState = appPrefs.getString("toggleState", "false");

            if(AppSettings.IS_LOG_ALLOWED) Log.e("toggle", toggleState);
            if (toggleState.equals("true")) {

                String q = "SELECT cts_number FROM contact_tosend";

                Cursor cur = db.selectQuery(q);

                if(AppSettings.IS_LOG_ALLOWED) Log.e("CURSOR COUNT", "" + cur.getCount());
                if (cur.getCount() > 0) {
                    if (cur.moveToFirst()) {
                        do {
                            String s = cur.getString(0);
                            arraylist.add(new Contacts(s));
                        } while (cur.moveToNext());

                    }
                    if(AppSettings.IS_LOG_ALLOWED) Log.e("arraysize2", "" + arraylist.size());
                }

            }


            for (int i = 0; i < sms_from_device.size(); i++) {
                Sms sms = sms_from_device.get(i);

                Date d = new Date(sms.receivedDate);

                String str_address = sms.address;
                String str_name = SmsReceiver.getContactNameByNumber(ctx, str_address);

                String str_msg_body = sms.body;

                String str_msg = "From: " + str_name + "  (" + str_address
                        + ")\r\nReceived at: " + d + "\r\n\r\nMessage: "
                        + str_msg_body;

//                sendSms(sms, str_msg);

                try {
                    msgsofcontacts_tosend(str_msg, sms.address);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
            writeLong(PREF_ID_SMS, sms_from_device.get(0).id);
        } else {
            if(AppSettings.IS_LOG_ALLOWED) Log.e("SF", "No message to forward");
        }
    }

    void msgsofcontacts_tosend(String str_msg, String number) {

        if (allmsg) {
            for (int i = 0; i < arraylist.size(); i++) {
                String phon = arraylist.get(i).getContact_number();
                sendSMS(phon, str_msg);
            }
        } else if (msg_from_contct) {
            boolean contactexist = contactExists(ctx, number);
            if (contactexist) {
                if(AppSettings.IS_LOG_ALLOWED) Log.e("arraysize", "" + arraylist.size());
                for (int i = 0; i < arraylist.size(); i++) {
                    String phon = arraylist.get(i).getContact_number();

                    sendSMS(phon, str_msg);
                }
            }

        } else if (msg_otherthen_cntct) {
            boolean contactexist = contactExists(ctx, number);
            if (!contactexist) {
                for (int i = 0; i < arraylist.size(); i++) {
                    String phon = arraylist.get(i).getContact_number();
                    sendSMS(phon, str_msg);
                }
            }
        } else if (msg_selectv) {
            String q2 = "SELECT cm_number FROM contact_msg";

            Cursor curs = db.selectQuery(q2);

            // for traversing
            if (curs.getCount() > 0) {
                if (curs.moveToFirst()) {
                    do {
                        String s = curs.getString(0);
                        selective_contact.add(new Contacts(s));
                    } while (curs.moveToNext());
                }
            }
            Contacts temp = new Contacts(number);

            for (int i = 0; i < selective_contact.size(); i++) {
                if (PhoneNumberUtils.compare(selective_contact.get(i)
                        .getContact_number(), temp.getContact_number())) {
                    for (int i1 = 0; i1 < arraylist.size(); i1++) {
                        String phon = arraylist.get(i1).getContact_number();
                        sendSMS(phon, str_msg);
                    }
                }
            }

            // if (selective_contact.contains(temp)) {
            // for (int i = 0; i <= arraylist.size(); i++) {
            // String phon = arraylist.get(i).getContact_number()
            // .toString();
            // sendSMS(phon, str_msg);
            // }
            //
            // }
        }
    }

    public static void writeLong(String key, long value) {
        myEditSpot.putLong(key, value);
        myEditSpot.commit();
    }

    private void sendSms(final Sms sms, final String msg) {
        SmsManager smsManager = SmsManager.getDefault();

        int iUniqueId = (int) (System.nanoTime() & 0xfffffff);
//        if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId", "" + iUniqueId);
        int iUniqueId2 = (int) (System.nanoTime() & 0xfffffff);
//        if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId2", "" + iUniqueId2);

//        Intent sentIntent = new Intent(SMS_SENT);
//        sentIntent.putExtra("sms_id", sms.id);
//        PendingIntent sentPIntent = PendingIntent.getBroadcast(ctx, iUniqueId, sentIntent, 0);
//
//        Intent deliveredIntent = new Intent(SMS_DELIVERED);
//        deliveredIntent.putExtra("sms_id", sms.id);
//        PendingIntent deliveredPIntent = PendingIntent.getBroadcast(ctx, iUniqueId2, deliveredIntent, 0);

        if (msg.length() > 155) {
            ArrayList<String> dividedMsg = smsManager.divideMessage(msg);
            ArrayList<PendingIntent> sendPiList = new ArrayList<>();
            ArrayList<PendingIntent> deliverPiList = new ArrayList<>();

            for (int i = 0; i < dividedMsg.size(); i++) {
                iUniqueId = (int) (System.nanoTime() & 0xfffffff);
                if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId", "" + iUniqueId);

                iUniqueId2 = (int) (System.nanoTime() & 0xfffffff);
                if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId2", "" + iUniqueId2);

//                sentIntent = new Intent(SMS_SENT);
//                sentIntent.putExtra("sms_id", sms.id);
//                sentPIntent = PendingIntent.getBroadcast(ctx, iUniqueId, sentIntent, 0);
//
//                deliveredIntent = new Intent(SMS_DELIVERED);
//                deliveredIntent.putExtra("sms_id", sms.id);
//                deliveredPIntent = PendingIntent.getBroadcast(ctx, iUniqueId2, deliveredIntent, 0);

//                sendPiList.add(sentPIntent);
//                deliverPiList.add(deliveredPIntent);
            }
            smsManager.sendMultipartTextMessage(sms.address, null, dividedMsg, sendPiList, deliverPiList);
        } else {
            PendingIntent deliveredPIntent = null;
            PendingIntent sentPIntent = null;

            smsManager.sendTextMessage(sms.address, null, msg, sentPIntent, deliveredPIntent);
        }
    }

    private void sendSMS(final String number, final String msg) {
        SmsManager smsManager = SmsManager.getDefault();

        int iUniqueId = (int) (System.nanoTime() & 0xfffffff);
//        if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId", "" + iUniqueId);
        int iUniqueId2 = (int) (System.nanoTime() & 0xfffffff);
//        if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId2", "" + iUniqueId2);

//        Intent sentIntent = new Intent(SMS_SENT);
//        sentIntent.putExtra("sms_id", sms.id);
//        PendingIntent sentPIntent = PendingIntent.getBroadcast(ctx, iUniqueId, sentIntent, 0);
//
//        Intent deliveredIntent = new Intent(SMS_DELIVERED);
//        deliveredIntent.putExtra("sms_id", sms.id);
//        PendingIntent deliveredPIntent = PendingIntent.getBroadcast(ctx, iUniqueId2, deliveredIntent, 0);

        if (msg.length() > 155) {
            ArrayList<String> dividedMsg = smsManager.divideMessage(msg);
            ArrayList<PendingIntent> sendPiList = new ArrayList<>();
            ArrayList<PendingIntent> deliverPiList = new ArrayList<>();

            for (int i = 0; i < dividedMsg.size(); i++) {
                iUniqueId = (int) (System.nanoTime() & 0xfffffff);
                if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId", "" + iUniqueId);

                iUniqueId2 = (int) (System.nanoTime() & 0xfffffff);
                if(AppSettings.IS_LOG_ALLOWED) Log.e("iUniqueId2", "" + iUniqueId2);

//                sentIntent = new Intent(SMS_SENT);
//                sentIntent.putExtra("sms_id", sms.id);
//                sentPIntent = PendingIntent.getBroadcast(ctx, iUniqueId, sentIntent, 0);
//
//                deliveredIntent = new Intent(SMS_DELIVERED);
//                deliveredIntent.putExtra("sms_id", sms.id);
//                deliveredPIntent = PendingIntent.getBroadcast(ctx, iUniqueId2, deliveredIntent, 0);

//                sendPiList.add(sentPIntent);
//                deliverPiList.add(deliveredPIntent);
            }
            smsManager.sendMultipartTextMessage(number, null, dividedMsg, sendPiList, deliverPiList);
        } else {
            PendingIntent deliveredPIntent = null;
            PendingIntent sentPIntent = null;

            smsManager.sendTextMessage(number, null, msg, sentPIntent, deliveredPIntent);
        }
    }

    public static long getLong(String key) {
        return myPrefSpot.getLong(key, 0);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            scheduledExecutorService_SMSSending.shutdownNow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
