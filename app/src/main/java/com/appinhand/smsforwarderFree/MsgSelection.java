package com.appinhand.smsforwarderFree;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MsgSelection extends Activity {

    List<Contacts> contact_Arraylist;
    MsgSelection_Adapter ma;
    ListView listContacts;
    Context context;
    DataBaseManager db;
    ImageView back_button;
    static RadioButton all_msg_radio;
    RadioButton msg_frm_cntct_radio;
    RadioButton msg_otherthen_cntct_radio;
    RadioButton msg_selctv_radio;
    static String prefName = "pref";
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg_selection);

        db = new DataBaseManager(context);
        context = context;
        contact_Arraylist = new ArrayList<Contacts>();

        listContacts = (ListView) findViewById(R.id.listView);
        back_button = (ImageView) findViewById(R.id.header_icon_image);

        pb = findViewById(R.id.pb);

        all_msg_radio = (RadioButton) findViewById(R.id.all_msg_radio);
        msg_frm_cntct_radio = (RadioButton) findViewById(R.id.msg_from_contct_radio);
        msg_otherthen_cntct_radio = (RadioButton) findViewById(R.id.msg_otherthan_contct_radio);
        msg_selctv_radio = (RadioButton) findViewById(R.id.msg_fromselectv_contct_radio);
        // relatv1 = (RelativeLayout) findViewById(R.id.relative1);
        adWork();

        if (AppSettings.IS_LOG_ALLOWED) Log.e("size", "" + contact_Arraylist.size());
        listContacts.setAdapter(ma);
        // ma.notifyDataSetChanged();

        listContacts.setOnItemClickListener((OnItemClickListener) context);
        listContacts.setItemsCanFocus(false);
        listContacts.setTextFilterEnabled(true);

        Boolean allmsg = GetValueFromPrefrencesForBoolean(
                getApplicationContext(), prefName, "allmsg");
        if (allmsg == false) {
            all_msg_radio.setChecked(true);
        } else {
            all_msg_radio.setChecked(allmsg);
            setActivityResult(all_msg_radio.getText().toString());
        }

        all_msg_radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub
                WriteToPrefrencesForBoolean(getApplicationContext(), prefName,
                        "allmsg", all_msg_radio.isChecked());

                if (isChecked) {
                    listContacts.setVisibility(View.GONE);

                    setActivityResult(all_msg_radio.getText().toString());
                }
            }
        });

        msg_frm_cntct_radio
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        WriteToPrefrencesForBoolean(getApplicationContext(),
                                prefName, "msg_from_contct",
                                msg_frm_cntct_radio.isChecked());

                        if (isChecked) {
                            listContacts.setVisibility(View.GONE);

                            setActivityResult(msg_frm_cntct_radio.getText()
                                    .toString());
                        }
                    }
                });
        msg_otherthen_cntct_radio
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {

                        WriteToPrefrencesForBoolean(getApplicationContext(),
                                prefName, "msg_otherthen_cntct",
                                msg_otherthen_cntct_radio.isChecked());
                        // TODO Auto-generated method stub
                        if (isChecked == true) {
                            listContacts.setVisibility(View.GONE);
                            setActivityResult(msg_otherthen_cntct_radio
                                    .getText().toString());

                        }

                    }
                });

        msg_selctv_radio
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        // TODO Auto-generated method stub
                        WriteToPrefrencesForBoolean(getApplicationContext(),
                                prefName, "msg_selctv",
                                msg_selctv_radio.isChecked());
                        if (isChecked) {
                            new ContactsLoader().execute();

                            setActivityResult(msg_selctv_radio.getText()
                                    .toString());

                        }
                    }
                });

        back_button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Bundle b = new Bundle();
                b.putBoolean("sendto_back", true);
                Intent i = new Intent(MsgSelection.this, MainActivity.class);
                i.putExtras(b);
                startActivity(i);
                finish();

            }
        });


        Boolean msg_from_contct = GetValueFromPrefrencesForBoolean(
                getApplicationContext(), prefName, "msg_from_contct");

        if (msg_from_contct) {
            setActivityResult(msg_frm_cntct_radio.getText().toString());
        }
        msg_frm_cntct_radio.setChecked(msg_from_contct);

        Boolean msg_otherthen_cntct = GetValueFromPrefrencesForBoolean(
                getApplicationContext(), prefName, "msg_otherthen_cntct");
        if (msg_otherthen_cntct) {
            setActivityResult(msg_otherthen_cntct_radio.getText().toString());
        }
        msg_otherthen_cntct_radio.setChecked(msg_otherthen_cntct);

        listContacts.setVisibility(View.GONE);
        pb.setVisibility(View.GONE);

        Boolean msg_selectv = GetValueFromPrefrencesForBoolean(
                getApplicationContext(), prefName, "msg_selctv");
        if (msg_selectv) {
            setActivityResult(msg_selctv_radio.getText().toString());
        }
        msg_selctv_radio.setChecked(msg_selectv);


    }

    class ContactsLoader extends AsyncTask<Void, Void, List> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pb.setVisibility(View.VISIBLE);
            listContacts.setVisibility(View.GONE);
        }

        @Override
        protected List doInBackground(Void... voids) {
            contact_Arraylist = getAllContacts(getContentResolver());

            ma = new MsgSelection_Adapter(getApplicationContext(),
                    R.layout.msg_rowlayout, contact_Arraylist);

            return contact_Arraylist;
        }

        @Override
        protected void onPostExecute(List arrayList) {
            super.onPostExecute(arrayList);

            Runnable runnable = new Runnable() {
                public void run() {
                    pb.setVisibility(View.GONE);
                    listContacts.setVisibility(View.VISIBLE);
                }
            };

            new Handler().postDelayed(runnable, 1500);

            listContacts.setAdapter(ma);
        }

    }

    void setActivityResult(String resultData) {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.putExtra("text", resultData);
        setResult(Activity.RESULT_OK, i);
    }

    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putBoolean("sendto_back", true);
        Intent i = new Intent(MsgSelection.this, MainActivity.class);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    public static void WriteToPrefrencesForBoolean(Context context,
                                                   String prefName, String key, boolean value) {
        SharedPreferences sp = context.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    public static Boolean GetValueFromPrefrencesForBoolean(Context context,
                                                           String prefName, String key) {
        SharedPreferences sp = context.getSharedPreferences(prefName,
                Context.MODE_PRIVATE);
        return sp.getBoolean(key, false);
    }

    public List<Contacts> getAllContacts(ContentResolver cr) {

        List<Contacts> tempList = new ArrayList<Contacts>();
        contact_Arraylist.clear();
        Cursor phones = null;
        try {
            phones = cr.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    null, null, null);
            while (phones.moveToNext()) {
                String name = phones
                        .getString(phones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = phones
                        .getString(phones
                                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                Contacts contact = new Contacts(phoneNumber, name, false);

                boolean found = false;
                for (int i = 0; i < contact_Arraylist.size(); i++) {
                    if (PhoneNumberUtils.compare(contact_Arraylist.get(i).getContact_number(), contact.getContact_number())) {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    contact_Arraylist.add(contact);
            }
        } finally {
            phones.close();
        }

        for (int i = 0; i < contact_Arraylist.size(); i++) {
            String query = "SELECT cm_number FROM contact_msg WHERE cm_number = \""
                    + contact_Arraylist.get(i).getContact_number() + "\"";

            Cursor c = db.selectQuery(query);
            Contacts temp_Model;
            if (c.getCount() > 0) {

                temp_Model = new Contacts(contact_Arraylist.get(i)
                        .getContact_number(), contact_Arraylist.get(i)
                        .getName(), true);
            } else {
                temp_Model = new Contacts(contact_Arraylist.get(i)
                        .getContact_number(), contact_Arraylist.get(i)
                        .getName(), false);
            }

            tempList.add(temp_Model);
            c.close();
        }

        Collections.sort(tempList, new Comparator<Contacts>() {
            @Override
            public int compare(Contacts contacts, Contacts t1) {
                return contacts.getName().compareTo(t1.getName());
            }
        });

        return tempList;
    }

    private List<Contacts> removeDuplicates(List<Contacts> contact_arraylist) {
        List<Contacts> noRepeat = new ArrayList<>();

        for (Contacts contact : contact_arraylist) {
            boolean isFound = false;
            if (!isFound) noRepeat.add(contact);
        }
        return null;
    }

    void adWork() {
        // Admob
        String adMobPulisherId = "ca-app-pub-9381472359687969/3946905736";
        String testingDeviceId = "359918043312594";
        AdView adView;
        RelativeLayout adlayout;
        AdRequest request;
        String publisherId = adMobPulisherId;
        try {
            adlayout = (RelativeLayout) findViewById(R.id.adLayout);
            // Get Screen
            Display display = ((WindowManager) this
                    .getSystemService(Context.WINDOW_SERVICE))
                    .getDefaultDisplay();
            int screenWidth = display.getWidth();
            if (AppSettings.IS_LOG_ALLOWED) Log.e("Screen Width", "" + screenWidth);

            // AdMob Size Initialisations
            if (screenWidth > 300 && screenWidth <= 320)
                adView = new AdView(this, AdSize.BANNER, publisherId);
            else
                adView = new AdView(this, AdSize.SMART_BANNER, publisherId);

            adlayout.addView(adView);

            // AdMob Request
            request = new AdRequest();

            // only for testing Devices
            request.addTestDevice(AdRequest.TEST_EMULATOR);
            request.addTestDevice(testingDeviceId);
            adView.loadAd(request);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
